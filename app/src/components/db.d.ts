import { ZoomSensePlugin } from "@zoomsense/zoomsense-firebase";
import { Teams } from "./types";

declare module "@zoomsense/zoomsense-firebase" {
  interface ZoomSenseDataPlugins {
    teamPlugin: {
      [meetingId: string]: Teams;
    };
  }
}
